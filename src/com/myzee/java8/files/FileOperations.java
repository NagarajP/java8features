package com.myzee.java8.files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.WatchService;

public class FileOperations {

	public enum PAGE {
		SIGN_CREATE0(0), SIGN_CREATE(1), HOME_SCREEN(2), REGISTER_SCREEN(3);

		private int id;

		PAGE(int id) {
			this.id = id;
		}

		public int getID() {
			return id;
		}
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		/*
		 * List all files and sub-directories using Files.list()
		 */

		System.out.println("List all files and sub-directories using Files.list()");
		Files.list(Paths.get("..\\MyFirstAngularApp")).forEach(System.out::println);
		;
		Files.list(Paths.get("..\\..")).forEach(System.out::println);
		;
		Files.list(Paths.get("C:\\Users\\nagaraj.pattar")).forEach(System.out::println);
		;

		/*
		 * List only files inside directory using filter expression
		 */

		System.out.println("\nList only files inside directory using filter expression");
		Files.list(Paths.get(".")).filter(Files::isRegularFile).forEach(System.out::println);
		/*
		 * Iterating a larger directory
		 */
		System.out.println("\nIterating a larger directory : Using Files.newDirectoryStream()");
		Files.newDirectoryStream(Paths.get(".")).forEach(System.out::println);

		/*
		 * Iterating through hidden files
		 */
		System.out.println("Iterating through hidden files: Using Files.isHidden()");
		File[] files = new File("C:\\Users").listFiles(file -> file.isHidden());
		// or
		File[] files1 = new File("C:\\Users\\").listFiles(File::isHidden);
		System.out.println(new StringBuilder("emi").reverse() + " for 9 - " + 46000 / 9);

		final Integer NOOFDAYS = 31;
		final Integer FINALCOUNT = 31 + 3;
		final Integer TOTALVAL = 122164;
		double perDayInc = TOTALVAL / FINALCOUNT;
		System.out.println(perDayInc + " and for 3d - " + perDayInc * 3);
		System.out.println(TOTALVAL - perDayInc * 3);
		System.out.println(PAGE.REGISTER_SCREEN.getID());

	}
}
