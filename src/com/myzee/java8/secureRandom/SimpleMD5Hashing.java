package com.myzee.java8.secureRandom;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SimpleMD5Hashing {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String passwordToHash = "password";
		String generatedPassword = null;
		try {
			// Create a MessageDigest instance for MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			
			// add password bytes to digest
			md.update(passwordToHash.getBytes());
			
			// get the hash bytes
			byte[] bytes = md.digest();
			System.out.println("password in bytes format - " + bytes);
			
			// this bytes[] has bytes in decimal format
			// convert it to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
				System.out.println(sb);
			}
			generatedPassword = sb.toString();
			System.out.println("\nMD5 ecnrypted password - " + generatedPassword);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
