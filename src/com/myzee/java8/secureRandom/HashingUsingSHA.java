
/*
 * Generate more secure encrypted string using SHA-1, SHA-256, SHA-384, SHA-512
 * Use SecureRandom to generate salt.
 */
package com.myzee.java8.secureRandom;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

public class HashingUsingSHA {

	public static void main(String[] args) {
		String passwordToHash = "password";
		String generatedPassword = null;
		String SHAType = null;
		byte[] salt = getSalt();
		try {
			SHAType = "SHA-1";
			generatedPassword = getSecurePasswordUsingSHA(passwordToHash, salt, SHAType);
			System.out.println("generated password using " + SHAType + " :\n\t" + generatedPassword);
			
			SHAType = "SHA-256";
			generatedPassword = getSecurePasswordUsingSHA(passwordToHash, salt, SHAType);
			System.out.println("\ngenerated password using " + SHAType + " :\n\t" + generatedPassword);
			
			SHAType = "SHA-384";
			generatedPassword = getSecurePasswordUsingSHA(passwordToHash, salt, SHAType);
			System.out.println("\ngenerated password using " + SHAType + " :\n\t" + generatedPassword);
			
			SHAType = "SHA-512";
			generatedPassword = getSecurePasswordUsingSHA(passwordToHash, salt, SHAType);
			System.out.println("\ngenerated password using " + SHAType + " :\n\t" + generatedPassword);
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static String getSecurePasswordUsingSHA(String passwordToHash, byte[] salt, String SHAType) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(SHAType);
		md.update(salt);
		byte[] bytes = md.digest(passwordToHash.getBytes());
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	/*
	 * generating salt using java.security.SecureRandom
	 */
	private static byte[] getSalt() {
		SecureRandom sr  = null;
		try {
			sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			e.printStackTrace();
		}
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return salt;
	}
}
