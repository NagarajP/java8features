package com.myzee.test;

public class EmployeeAddress {
	private String city;
	private String country;

	public EmployeeAddress() {
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}