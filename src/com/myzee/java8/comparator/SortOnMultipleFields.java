package com.myzee.java8.comparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class SortOnMultipleFields {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Sorting based on multiple fields using Comparator.comparing() and Comparator.thenComparting()
		 */
		System.out.println("Comparator.comparing() and Comparator.thenComparing()");
		List<User> uli = User.getUDet();
		Comparator<User> comp = Comparator.comparing(User::getU_id).thenComparing(User::getU_name);
		uli.sort(comp);
		uli.forEach(u -> System.out.println(
				u.getU_id() + ", " +
				u.getU_name() + ", " +
				u.getU_sid()));
		
	}
}

class User {
	private int u_id;
	private String u_name;
	private String u_sid;
	public int getU_id() {
		return u_id;
	}
	public void setU_id(int u_id) {
		this.u_id = u_id;
	}
	public String getU_name() {
		return u_name;
	}
	public void setU_name(String u_name) {
		this.u_name = u_name;
	}
	public String getU_sid() {
		return u_sid;
	}
	public void setU_sid(String u_sid) {
		this.u_sid = u_sid;
	}
	
	public static List<User> getUDet() {
		List<User> uli = new ArrayList<>();
		for(int i = 0; i < 10; i++) {
			User u = new User();
			u.setU_id(new Random().nextInt(100));
			u.setU_name("tav" + i * 5);
			u.setU_sid(i + "");
			uli.add(u);
		}
		return uli;
	}
	
}

