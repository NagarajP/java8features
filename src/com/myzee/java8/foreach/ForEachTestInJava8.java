package com.myzee.java8.foreach;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ForEachTestInJava8 {

	public static void main(String[] args) {
		
		// Traversing map through forEach loop
		System.out.println("Traversing map through forEach loop");
		Map<Integer, String> m = new HashMap();
		m.put(1, "one");
		m.put(2, "two");
		m.put(3, "three");
		m.forEach(
				(k,v)-> 
				{
					System.out.println("key - " + k + " value - " + v);
				}
				);
		
		// Traversing map through forEach using EntrySet.
		m.entrySet().forEach(e -> {
			System.out.println(e.getKey());
			System.out.println(e.getValue());
			});
		
		// Traversing List using forEach loop 
		System.out.println("Traversing List through forEach loop");
		List<Integer> l = new ArrayList<Integer>();
		l.add(1);
		l.add(2);
		l.add(3);
		l.forEach(
				(item) -> {
					System.out.println(item);
					}
				);
		
		// Traversing Set using forEach loop
		System.out.println("Traversing Set through forEach loop");
		Set<String> s = new HashSet<String>();
		s.add("one");
		s.add("two");
		s.add("three");
		s.add("three");	//duplicates not allowed
		s.forEach(
				(element) -> {
					System.out.println(element);
					}
				);
	}
}
