package com.myzee.java8.stream.map;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamMapMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Worker> list = Arrays.asList(
					new Worker(1, "mark", "markhome"),
					new Worker(1, "hood", "hoodhome"),
					new Worker(1, "jenhi", "jenhihome")
				);
		
		//mapping from List<Worker> to List<String> using stream.map()
		
		List<String> nameList = list.stream().map(Worker::getName).collect(Collectors.toList());
		System.out.println(nameList);
	}
}

class Worker {
	int id;
	String name;
	String address;
	
	public Worker(int id, String name, String address) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
}
