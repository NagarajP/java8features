package com.myzee.lambdatest;

@FunctionalInterface
interface Inter1 {
	public void print();
}

class Demo implements Inter1 {
	public void print() {
		System.out.println("say hello to functinal interface");
	}
}

@FunctionalInterface
interface Inter2 {
	public int add(int a, int b);
}

class Demo1 implements Inter2 {
	public int add(int a, int b) {
		return a+b;
	}
}

public class FuncInterfaceWithoutUsingLambda {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Inter1 i1 = new Demo();
		i1.print();		// it calls the method and execute correctly
		
		/*
		 * using traditional way, not using Lambda
		 */
		Inter2 i2 = new Demo1();
		int sum = i2.add(10,  20);
		System.out.println("sum is = " + sum);
		
		/*
		 * equivalent code using Lambda expression from java1.8
		 */
		Inter2 ii = (a, b) -> a+b;
		int sum1 = ii.add(20, 20);
		System.out.println(sum1);
		
		
	}

}


