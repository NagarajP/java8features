package com.myzee.java8.methodreference;

public class InstanceMethodReference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Using lambda expression");
		IntanceRefClass ic = new IntanceRefClass();
		InstanceRef i = () -> ic.instanceMethRefClass();
		i.intanceRefMethodInInterface();
		
		//or
		System.out.println("Using method reference");
		IntanceRefClass ic1 = new IntanceRefClass();
		InstanceRef i1 = ic1::instanceMethRefClass;
		i1.intanceRefMethodInInterface();
	}

}

class IntanceRefClass {
	public void instanceMethRefClass() {
		System.out.println("inside instanceMethRef() of class");
	}
}

@FunctionalInterface
interface InstanceRef {
	public void intanceRefMethodInInterface();
}
