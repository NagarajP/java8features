package com.myzee.java8.intsummarystatistics;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.Iterator;
import java.util.List;

public class IntSummaryStatisticsDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		IntSummaryStatistics intSummaryStatistics = new IntSummaryStatistics();
		List<Integer> intList = Arrays.asList(10, 20, 30, 40, 50);
		
		Iterator<Integer> itr = intList.iterator();
		while (itr.hasNext()) {
			Integer value = (Integer) itr.next();
			intSummaryStatistics.accept(value);
		}
		
		System.out.println("sum is - " + intSummaryStatistics.getSum());
		System.out.println("average is - " + intSummaryStatistics.getAverage());
		System.out.println("count is - " + intSummaryStatistics.getCount());
		System.out.println("max number in list  is - " + intSummaryStatistics.getMax());
		System.out.println("minimum number in list  is - " + intSummaryStatistics.getMin());
		
	}

}
