package com.myzee.java8.foreach;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import javax.sound.midi.Soundbank;

public class ForEach {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> al = new ArrayList<>(Arrays.asList(1,2,3,4,5,6));
		Consumer<Integer> action = System.out::println;
		System.out.println("All elements");
		al.stream().forEach((n) -> System.out.println(n));
		System.out.println("Even elements only");
		al.stream().filter(n -> n%2 == 0).forEach(action);
		
		System.out.println("forEach on Map");
		Map<String, Integer> m = new HashMap<>();
		m.put("a", 97);
		m.put("b", 98);
		m.put("c", 99);
		System.out.println();
		Consumer<Map.Entry<String, Integer>> actionMap = System.out::println;
		m.entrySet().forEach(actionMap);
		System.out.println();
		/*
		 * print keys using foreach and action - java8
		 */
		Consumer<String> actionKeys = System.out::println;
		m.keySet().forEach(actionKeys);
		//get the value of key or default mentioned in below method
		System.out.println(m.getOrDefault("z", 99999));
		
		//put the key and value entry into map if its not present
		m.putIfAbsent("d", 100);
		
		System.out.println();
		Consumer<Integer> actionValues = System.out::println;
		m.values().forEach(actionValues);
		/*
		 * map display
		 */
		System.out.println(m);
		
	}
}
