package com.myzee.funinterfacelambda;

public class LambdaWithMultParams {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FunInterface1 add = (int x, int y) -> x+y;
		FunInterface1 mult = (int x, int y) -> x*y;
		System.out.println(add.operate(2, 5));
		System.out.println(mult.operate(2, 5));
		System.out.println(add.done(0, 0, 0));
	}

}

interface FunInterface1 {
	abstract int operate(int x, int y);
//	abstract void nothing(String m, String n);	// uncomment this line and check
	default int done(int i, int j, int k) {return 99;};
}
