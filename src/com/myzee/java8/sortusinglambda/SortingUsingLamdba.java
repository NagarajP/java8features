package com.myzee.java8.sortusinglambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortingUsingLamdba {

	public static void main(String[] args) {
		/*
		 * Without using lambda
		 */
		System.out.println("Without using lambda");
		List<PC> pclist = getPc();
		Collections.sort(pclist, new Comparator<PC>() {

			@Override
			public int compare(PC pc1, PC pc2) {
				// TODO Auto-generated method stub
				return pc1.getSerial()-pc2.getSerial();
			}
			
		});
		showPc(pclist);
		/*
		 * With using lambda
		 */
		System.out.println("\nWith using lambda, descending order");
		List<PC> pclist1 = getPc();
		pclist1.sort((PC pc1, PC pc2) -> pc2.getSerial() - pc1.getSerial());
		//or
		pclist1.sort((pc1, pc2) -> pc1.getSerial()-pc2.getSerial()); //type is optional
		showPc(pclist1);
		
		/*
		 * sort by brand, using lambda 
		 */
		System.out.println("\nSort using brand");
		pclist1.sort((pc1, pc2) -> pc1.getBrand().compareTo(pc2.getBrand()));
		showPc(pclist1);
		
		/*
		 * Using Comparator instance.
		 */
		System.out.println("\nUsing Comparator instance.");
		Comparator<PC> modelComparator = ((pc1, pc2) -> (pc1.getModel().compareTo(pc2.getModel())));
		pclist1.sort(modelComparator.reversed());
		showPc(pclist1);
		
		//stream and filter
		System.out.println("\nFilter list, vomit acer from the list");
		List<PC> filterList = pclist1.stream().filter(pc -> !"Acer".equals(pc.getBrand())).collect(Collectors.toList());
		showPc(filterList);
		
	}
	
	private static List<PC> getPc() {
		List<PC> pclist = Arrays.asList(new PC(1001, "Z5111", "Dell"), 
				new PC(1004, "J5111", "HP"), 
				new PC(1002, "B5111", "Acer"),
				new PC(1003, "Yp5111", "Apple"));
		return pclist;
	}
	private static void showPc(List<PC> pclist) {
		pclist.forEach(pc -> System.out.println(pc.getSerial() + "\t" + pc.getModel() + "\t" + pc.getBrand()));
	}

}

class PC {
	int serial;
	String model;
	String brand;
	
	public PC( int serial, String model, String brand) {
		// TODO Auto-generated constructor stub
		this.serial = serial;
		this.model = model;
		this.brand = brand;
	}

	public int getSerial() {
		return serial;
	}

	public String getModel() {
		return model;
	}

	public String getBrand() {
		return brand;
	}
	
}
