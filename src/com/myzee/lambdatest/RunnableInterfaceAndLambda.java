package com.myzee.lambdatest;

class MyRunnable implements Runnable {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 10; i++) {
			System.out.println("Child Thread");
		}
	}
	
}

public class RunnableInterfaceAndLambda {

	public static void main(String[] args) {

		/*
		 * creating thread without using lambda expressions
		 */
		MyRunnable mr = new MyRunnable();
		Thread t = new Thread(mr);
		t.start();
		for(int i = 0; i < 10; i++) {
			System.out.println("Main Thread");
		}
		
		/*
		 * calling Runnable run() method using lambda while creating thread
		 */
		Runnable r = () -> {
			for(int i = 0; i < 10; i ++) {
				System.out.println("thread using lambda");
			}
		};
		Thread t1 = new Thread(r);
		t1.start();
	}
}


