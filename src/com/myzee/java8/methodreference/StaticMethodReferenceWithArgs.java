package com.myzee.java8.methodreference;
/*
 * Reference : https://youtu.be/S5zVwjtMxoc
 * Durga soft
 */
public class StaticMethodReferenceWithArgs {

	public static void main(String[] args) {
		
		Greeting g  = (str) -> AlternateGreeting.alternateMessage(str);
		g.message("with parameter : using lambda ");
		
		Greeting g1 = AlternateGreeting::alternateMessage;
		g1.message("with parameter : using static reference ");
	}

}

class AlternateGreeting {
	public static void alternateMessage(String message) {
		System.out.println("in alternateMessage() method - " + message);
	}
}

@FunctionalInterface
interface Greeting {
	public void message(String str);
}
