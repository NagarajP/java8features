package com.myzee.predicate;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PredicateDotAnd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> l = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		l.stream().filter(i -> i > 4 && i < 8).forEach(i -> System.out.print(i + " "));
		
		System.out.println();
		
		// Using Predicate.and()
		Predicate<Integer> greaterThan = i -> i > 4;
		Predicate<Integer> lessThan = i -> i < 8;
		
		l.stream().filter(greaterThan.and(lessThan)).forEach(i -> System.out.print(i + " "));;
		
		System.out.println("// Using Predicate.and()");
		// Using Predicate.and()
		
		List<Integer> li = l.stream().filter(greaterThan.and(lessThan)).collect(Collectors.toList());
		li.forEach(x -> System.out.print(x + ", "));
	}

}
