package com.myzee.java8.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
//import java.lang.System.*;
public class LambdaAndComparator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * sort using lambda and functional interface(Comparator)
		 */
		System.out.println("sort using lambda and functional interface(Comparator)");
		Users sortUs = new Users();
		List<Users> sortUli = sortUs.getUDet();
		
		System.out.println("\nSort using uid");
		sortUli.sort(Comparator.comparing(u -> u.getU_id()));
		sortUli.forEach(u -> System.out.println(u.getU_id() + ", " + u.getU_name() + ", " + u.getU_sid()));
	
		System.out.println("\nSort using uname");
		sortUli.sort(Comparator.comparing(Users::getU_name));
		sortUli.forEach(u -> System.out.println(u.getU_id() + ", " + u.getU_name() + ", " + u.getU_sid()));
		
		System.out.println("\nSort using usid and in reversed");
		Comparator<Users> urev = Comparator.comparing(Users::getU_sid);
		sortUli.sort(urev.reversed());
		sortUli.forEach(u -> System.out.println(u.getU_id() + ", " + u.getU_name() + ", " + u.getU_sid()));
//		Collections.sort(sortUli, urev);
	}
}


class Users {
	private int u_id;
	private String u_name;
	private String u_sid;
	public int getU_id() {
		return u_id;
	}
	public void setU_id(int u_id) {
		this.u_id = u_id;
	}
	public String getU_name() {
		return u_name;
	}
	public void setU_name(String u_name) {
		this.u_name = u_name;
	}
	public String getU_sid() {
		return u_sid;
	}
	public void setU_sid(String u_sid) {
		this.u_sid = u_sid;
	}
	
	public List<Users> getUDet() {
		List<Users> uli = new ArrayList<>();
		for(int i = 0; i < 10; i++) {
			Users u = new Users();
			u.setU_id(new Random().nextInt(100));
			u.setU_name("tav" + i * 5);
			u.setU_sid(i + "");
			uli.add(u);
		}
		return uli;
	}
	
}

