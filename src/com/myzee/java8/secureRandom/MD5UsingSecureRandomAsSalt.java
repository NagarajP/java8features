package com.myzee.java8.secureRandom;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/*
 * Make MD5 more secure using salt.
 * 
 * Important: We always need to use a SecureRandom to create good salts, 
 * and in Java, the SecureRandom class supports the �SHA1PRNG� pseudo random number generator 
 * algorithm, and we can take advantage of it.
 */
public class MD5UsingSecureRandomAsSalt {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String passwordToHash = "password";
		String generatedPassword = null;
		// generate salt using SecureRandom
		byte[] salt = getSalt();
		// generate more secure MD5 password using salt
		generatedPassword = getSecurePassword(passwordToHash, salt);
		System.out.println("password with salt - " + generatedPassword);
		
		String regeneratedPasswordtoVerify = getSecurePassword(passwordToHash, salt);
		System.out.println("verify - " + regeneratedPasswordtoVerify);
	}

	private static byte[] getSalt() {
		// TODO Auto-generated method stub
		try {
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
			byte[] salt = new byte[16];
			sr.nextBytes(salt);
			return salt;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static String getSecurePassword(String passwordToHash, byte[] salt) {
		// TODO Auto-generated method stub
		byte[] bytes = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(salt);
			bytes = md.digest(passwordToHash.getBytes());
			System.out.println(bytes);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		
		return sb.toString();
	}
	

}
