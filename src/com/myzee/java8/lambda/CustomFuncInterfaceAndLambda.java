package com.myzee.java8.lambda;

public class CustomFuncInterfaceAndLambda {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * implementing call method of functional interface, below.
		 * (String st) -> System.out.println("Functional Interface call method")
		 */
		MyFuncInterface s = (String str) -> System.out.println(str);
		
		//access call() method
		s.call("Functional Interface");
		s.call("testing functional interface ");
		s.call("working fine");
	}

}

@FunctionalInterface
interface MyFuncInterface {
	
	public void call(String str);
	
	/*
	 * default method in interface
	 */
	public default void defaultCall(String str){
		
	}
}