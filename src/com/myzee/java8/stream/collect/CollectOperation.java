package com.myzee.java8.stream.collect;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

public class CollectOperation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<Integer, Person> pmap = new HashMap<>();
		pmap.put(1, new Person(1, "bobby", "yohn", "MALE"));
		pmap.put(2, new Person(2, "Kathie", "seira", "FEMALE"));
		pmap.put(3, new Person(3, "Rob", "stark", "MALE"));
		pmap.put(4, new Person(4, "mark", "leon", "MALE"));

		// fetching the only male sex persons and making list on their firstnames
		// return : list of persons firstName
		System.out.println("\nFirstNames list");
		List<String> malelist = pmap.entrySet().stream().filter(p -> p.getValue().getSex().equals("MALE")).map(p -> p.getValue().getFname())
				.collect(Collectors.toList());
		malelist.forEach(System.out::println);
		//-----------------------------------------------------
		List<Person> plist = Arrays.asList(
				new Person(1, "bobby", "yohn", "MALE"),
				new Person(2, "Kathie", "seira", "FEMALE"),
				new Person(3, "Rob", "stark", "MALE"),
				new Person(4, "mark", "leon", "MALE")
				);
		
		//-------------------------------------------------------
		System.out.println("\n\nLast name list");
		List<String> lastNameList = plist.stream().filter(p -> p.getSex() == "MALE").map(p -> p.getLname()).collect(Collectors.toList());
		lastNameList.forEach(System.out::println);
		
		
		pmap.entrySet().stream().filter(p -> p.getValue().getSex() == "MALE").collect(Collectors.toMap(Person::getId, Person::getFname));
		
	}

}

@AllArgsConstructor
@Getter
@Setter
class Person {
	private int id;
	private String fname;
	private String lname;
	private final String sex;
}
