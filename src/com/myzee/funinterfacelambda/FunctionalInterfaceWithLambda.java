/*
 * Funcational interface is the one which have only one abstract method.
 * Such method can be called directly using lambda expressions.
 */
package com.myzee.funinterfacelambda;

@FunctionalInterface
interface FunInterface {
	public void abstractMeth(String str);
//	public void meth1(); 	// The target type of this expression must be a functional interface
	default void normalMethod(String str) {
		System.out.println(str);
	}
}

public class FunctionalInterfaceWithLambda {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FunInterface funObj = (String str)-> System.out.println(str);
		funObj.abstractMeth("call to functional interface method");
		funObj.normalMethod("normal method : default method");
	}
}
