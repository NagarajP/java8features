package com.myzee.predicate;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class UsingPredicate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> l = Arrays.asList(1,2,3,4,5,6,7,8,9);
		l.stream().filter(i -> i > 5).forEach(x -> System.out.print(x + " "));
		
		//Using predicate to create the same condition.
		System.out.println();
		Predicate<Integer> greaterThan = i -> i > 5;
		l.stream().filter(greaterThan).forEach(x -> System.out.print(x + " "));
	}

}
