package com.myzee.java8.optional;

import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Optional;

public class OptionalTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		Optional<Integer> o = Optional.ofNullable(null);
		Optional<Integer> o = Optional.ofNullable(7);
		System.out.println(o.equals(null));
		System.out.println(o.isPresent());
		//orElse will return the value ifPresent, else return the other mentioned value
		System.out.println(o.orElse(new Integer(9999)));
		System.out.println("---------------------------------------------------");
		Optional<Company> oc = Optional.empty();
		Company def = oc.orElse(new Company("def"));
		System.out.println(def.name);
		/*
		 * Optional.orElseThrow(Supplier<? extends Throwable> suplierException) using
		 */
		System.out.println("Using, Optional.orElseThrow(Supplier<? extends Throwable> supplierException)");
		Optional<Company> oc1 = Optional.ofNullable(null);
//		oc1.orElseThrow(NullPointerException::new);
		
		Optional<Company> oco = Optional.of(new Company("Fin"));
		oco.filter(n -> "Fin".equals(n.name)).ifPresent((n -> System.out.println(n.name + " is present")));
		Comparable<Integer> co = new Comparable<Integer>() {
			
			@Override
			public int compareTo(Integer o) {
				// TODO Auto-generated method stub
				return 0;
			}
		};
		/*
		 * NoSuchElementException: No value present
		 */
		Optional<Integer> oi = Optional.ofNullable(null);
		System.out.println(oi.get());
	}

}

class Company {
	String name;
	public Company(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
		System.out.println("inside company constructor");
		Optional<String> os = Optional.of("one");
		Optional.of("two");
		System.out.println(os.get());
	}
}