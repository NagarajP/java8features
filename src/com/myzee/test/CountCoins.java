
// Not working
package com.myzee.test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class CountCoins {
	static int cnt = 0;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> cns = Arrays.asList(1, 2);
		int combCnt = countRupeeNotes(4, cns);
		System.out.println(combCnt);
	}

	public static Integer countRupeeNotes(Integer money, List<Integer> coins) {
		// TODO Auto-generated method stub
		Iterator<Integer> itr = coins.iterator();
		while(itr.hasNext()) {
			int coin = itr.next();
			if(coin <= money) {
				if (money%coin == 0) {
					cnt++;
				} else {
					countRupeeNotes(money%coin, coins);
				}
			}
		}
		return cnt;
	}
}
