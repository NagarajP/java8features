package com.myzee.java8.foreach;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class CustomActionforForEach {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("forEach on Map");
		Map<String, Integer> m = new HashMap<>();
		m.put("a", 97);
		m.put("b", 98);
		m.put("c", 99);
		System.out.println();
		
		/*
		 * Creating custom action
		 */
		Consumer<Map.Entry<String, Integer>> action = entry -> {
			System.out.print("K = " + entry.getKey() + ", ");
			System.out.println("V = " + entry.getValue());
		};
		
		System.out.println(action);
		
		/*
		 * traverse using java8 foreach
		 */
		m.entrySet().forEach(action);
		m.entrySet().forEach(e -> {System.out.print(e.getKey() + "----"); System.out.println(e.getValue());});
	}
}
