package com.myzee.java8.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CreateStream {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Using Stream.of(val1, val2, val3�.)
		 */
		System.out.println("Using Stream.of(val1, val2, val3�.)");
		Stream<Integer> stream = Stream.of(1,2,3,4,5);
		stream.forEach(p -> System.out.println(p));
		
		/*
		 * Using Stream.of(arrayOfElements)
		 */
		System.out.println("Using Stream.of(arrayOfElements)");
		Stream<Integer> streamArr = Stream.of(new Integer[] {1,2,3,5,6,7,8});
		streamArr.forEach(p -> System.out.println(p));
		
		/*
		 * Using List.stream()
		 */
		System.out.println("Using List.stream()");
		List<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5,6));
		Stream<Integer> streamList = list.stream();
		streamList.forEach(p -> System.out.println(p));
		
		/*
		 * Using List.stream().sorted()
		 */
		System.out.println("Using List.stream().sorted()");
		List<String> stlist = new ArrayList<>(Arrays.asList("bob", "alice", "zenson", "ketherine"));
		stlist.stream().sorted().forEach(System.out::println);
		
		System.out.println("\nto upper case\n---------------");
		Stream<String> getStream = stlist.stream();
		Stream<String> sortedStream = getStream.sorted();
		Stream<String> upperStream = sortedStream.map(String::toUpperCase);
		upperStream.forEach(System.out::println);
		/*
		 * run time error in below line.
		 *  java.lang.IllegalStateException: stream has already been operated upon or closed
		 */
//		long count = upperStream.count();
//		System.out.println("Stream.count() - " + count);
		
		long countList = stlist.stream().sorted().map(String::toUpperCase).count();
		System.out.println(countList);
		
		System.out.println("only even");
		List<Integer> ilist = Arrays.asList(10,4,1,9,3);
		Predicate<Integer> p = n -> (n % 2 != 0);
		ilist = ilist.stream().filter(p).collect(Collectors.toList());
		ilist.stream().forEach(System.out::println);
		
	}
}
